package com.project2224.locations.services

import com.project2224.locations.models.State
import com.project2224.locations.repositories.{StateEntity, StateRepository}

class StateService(stateRepository: StateRepository) {
  def getById(id: Int): State = {
    val entity = stateRepository.getById(id)
    modelFromEntity(entity.get)
  }

  def modelFromEntity(entity: StateEntity): State = {
    State(entity.id, entity.code, entity.name, entity.comments, entity.extendedComments)
  }
}
