package com.project2224.locations.services

import com.project2224.locations.models.{District, DistrictRace, State}
import com.project2224.locations.repositories.{BallotAccessRepository, DistrictEntity, DistrictRaceRepository, DistrictRepository, WriteInAccessRepository}

class DistrictService(
                     districtRepository: DistrictRepository,
                     stateService: StateService
                     ) {
  def getById(id: Int): District = {
    val entity: DistrictEntity = districtRepository.getById(id).get
    val state = stateService.getById(entity.stateId)
    modelFromEntityAndRelation(entity, state)
  }

  def modelFromEntityAndRelation(entity: DistrictEntity, state: State): District = {
    District(entity.id, entity.districtId, state)
  }
}
