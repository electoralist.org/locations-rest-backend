package com.project2224.locations.controllers

import com.project2224.locations.services.DistrictRaceService
import org.json4s.{DefaultFormats, Formats}
import org.scalatra.ScalatraServlet
import org.scalatra.json.JacksonJsonSupport

class DistrictController(
                          districtRaceService: DistrictRaceService
                        ) extends ScalatraServlet with JacksonJsonSupport {
  protected implicit lazy val jsonFormats: Formats = new DefaultFormats {} + SimpleDateFormatter

  before() {
    contentType = formats("json")
  }

  get("/races/byState/:stateCode") {
    val stateCode = params("stateCode")
    districtRaceService.getByStateCode(stateCode)
  }

  get("/races/:districtRaceId") {
    val districtRaceId = Integer.parseInt(params("districtRaceId"))
    districtRaceService.getById(districtRaceId)
  }

}
