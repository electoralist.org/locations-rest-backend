package com.project2224.locations.controllers

import org.json4s.{DefaultFormats, Formats}
import org.scalatra._
import org.scalatra.json.JacksonJsonSupport

import java.util.Date

case class Health(time: Date, version: String)

class HealthController extends ScalatraServlet with JacksonJsonSupport {
  protected implicit lazy val jsonFormats: Formats = DefaultFormats

  before() {
    contentType = formats("json")
  }

  get("/") {
    Health(new Date(), "0.1.0")
  }

}
