package com.project2224.locations.repositories

import scalikejdbc._

case class DistrictRaceBallotAccessEntity(id: Int, districtRaceId: Int, ballotAccessId: Int)

class DistrictRaceBallotAccessRepository {
  def getDistrictRaceBallotccess(districtRaceId: Int): List[DistrictRaceBallotAccessEntity] = DB readOnly {
    implicit session =>
      sql"""select id, district_race_id, ballot_access_id from district_race_ballot_access
           where district_race_id = ${districtRaceId}"""
        .map(rs => DistrictRaceBallotAccessEntity(
          rs.int("id"),
          rs.int("district_race_id"),
          rs.int("ballot_access_id"))
        ).list.apply()
  }
}
