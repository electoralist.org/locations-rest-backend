package com.project2224.locations.repositories

import scalikejdbc._

import java.sql.Date

case class DistrictRaceEntity(id: Int, date: Date, district_id: Int, special: Boolean)

class DistrictRaceRepository {
  def getDistrictRaceByDistrictRaceId(districtRaceId: Int): Option[DistrictRaceEntity] = DB readOnly {
    implicit session =>
      sql"""select id, date, district_id, special
         from district_race where id = ${districtRaceId}"""
        .map(rs => DistrictRaceEntity(
          rs.int("id"),
          rs.date("date"),
          rs.int("district_id"),
          rs.boolean("special")
        )
        ).single.apply()
  }

  def getDistrictRacesByStateCode(stateCode: String): List[DistrictRaceEntity] = DB readOnly {
    implicit session =>
      sql"""select dr.id as id, dr.date as date, dr.district_id as district_id, dr.special as special
         from district_race dr inner join district d on dr.district_id = d.id inner join state s
         on d.state_id = s.id where s.code = ${stateCode}"""
        .map(rs => DistrictRaceEntity(
          rs.int("id"),
          rs.date("date"),
          rs.int("district_id"),
          rs.boolean("special")
        )
        ).list.apply()
  }
}
