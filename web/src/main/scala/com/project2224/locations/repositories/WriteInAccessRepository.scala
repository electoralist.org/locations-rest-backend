package com.project2224.locations.repositories

import scalikejdbc._

import java.sql.Date

case class WriteInAccessEntity(id: Int, name: String, filingDeadline: Date, filingFee: Int, signatureRequirement: Int, comments: Option[String], extendedComments: Option[String], source: Option[String])

class WriteInAccessRepository {
  def getDistrictRaceWriteInAccess(districtRaceId: Int): Option[WriteInAccessEntity] = DB readOnly {
    implicit session =>
      sql"""select
       wia.id as id,
       wia.name as name,
       wia.filing_deadline as filing_deadline,
       wia.filing_fee as filing_fee,
       wia.signature_requirement as signature_requirement,
       wia.comments as comments,
       wia.extended_comments as extended_comments,
       wia.source as source
       from ballot_access as wia
       inner join district_race_write_in_access drwia on drwia.write_in_access_id = wia.id
       where drwia.district_race_id = ${districtRaceId}"""
        .map(rs => WriteInAccessEntity(
          rs.int("id"),
          rs.string("name"),
          rs.date("filing_deadline"),
          rs.int("filing_fee"),
          rs.int("signature_requirement"),
          rs.getOpt[String]("comments"),
          rs.getOpt[String]("extended_comments"),
          rs.getOpt[String]("source")
        )
        ).single.apply()
  }
}
