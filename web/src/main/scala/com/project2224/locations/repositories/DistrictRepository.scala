package com.project2224.locations.repositories

import scalikejdbc._

import java.sql.Date

case class DistrictEntity(id: Int, districtId: Int, stateId: Int)

class DistrictRepository {
  def getDistrictsByStateId(stateId: Int): List[DistrictEntity] = DB readOnly {
    implicit session =>
      sql"""select id, district_number, state_id
         from district where state_id = ${stateId}"""
        .map(rs => DistrictEntity(
          rs.int("id"),
          rs.int("district_number"),
          rs.int("state_id")
        )
        ).list.apply()
  }

  def getById(districtId: Int): Option[DistrictEntity] = DB readOnly {
    implicit session =>
      sql"""select id, district_number, state_id
         from district where id = ${districtId}"""
        .map(rs => DistrictEntity(
          rs.int("id"),
          rs.int("district_number"),
          rs.int("state_id")
        )
        ).single.apply()

  }
}
