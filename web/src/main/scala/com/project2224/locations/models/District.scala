package com.project2224.locations.models

import java.util.Date


case class DistrictRace(
                         id: Int,
                         district: District,
                         date: Date,
                         special: Boolean,
                         ballotAccesses: List[BallotAccess],
                         writeInAccess: WriteInAccess
                       )

case class District(
                   id: Int,
                   districtNumber: Int,
                   state: State
                   )

case class DistrictList(
                       results: List[District]
                       )