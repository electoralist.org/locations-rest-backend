package com.project2224.locations.models

import java.util.Date

case class WriteInAccess(
                        id: Int,
                        name: String,
                        filingDeadline: Date,
                        filingFee: Int,
                        signatureRequirement: Int,
                        comments: Option[String],
                        extendedComments: Option[String],
                        source: Option[String]
                        )
