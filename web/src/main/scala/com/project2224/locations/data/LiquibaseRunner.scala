package com.project2224.locations.data

import liquibase.Liquibase
import liquibase.database.jvm.JdbcConnection
import liquibase.database.{Database, DatabaseFactory}
import liquibase.resource.ClassLoaderResourceAccessor

import java.sql.Connection

class LiquibaseRunner(connection: Connection) {
  def run(): Unit = {
    val database: Database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection))
    val liquibase = new Liquibase("main-changelog.xml", new ClassLoaderResourceAccessor(), database)
    liquibase.update("Staring up")
  }
}
