val ScalatraVersion = "2.8.2"

ThisBuild / scalaVersion := "2.13.7"
ThisBuild / organization := "com.project2224"


lazy val web = (project in file("web"))
  .settings(
    name := "locations-rest-backend-web",
    version := "0.1.0-SNAPSHOT",
    containerPort := 8082,
    libraryDependencies ++= Seq(
      "org.scalatra" %% "scalatra" % ScalatraVersion,
      "org.scalatra" %% "scalatra-scalatest" % ScalatraVersion % "test",
      "ch.qos.logback" % "logback-classic" % "1.2.3" % "runtime",
      "org.eclipse.jetty" % "jetty-webapp" % "9.4.35.v20201120" % "container",
      "javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",
      "org.scalatra" %% "scalatra-json" % "2.8.2",
      "org.json4s"   %% "json4s-jackson" % "4.0.1",
      "org.liquibase" % "liquibase-core" % "4.7.1",
      "com.zaxxer" % "HikariCP" % "5.0.1",
      "org.postgresql" % "postgresql" % "42.3.1",
      "org.scalikejdbc" %% "scalikejdbc" % "4.0.0"
    ),
  ).enablePlugins(JettyPlugin)

